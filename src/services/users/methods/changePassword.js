const bcrypt = require("bcrypt");
const Schema = require("../../../middleware/validation");

// http://localhost:3030/changePassword
// change User Password.
const changePassword = async (req, res, next) => {
  try {
    const { app, user, body } = req;
    //validate inputs using joi.
    const value = Schema.changePasswordSchema.validateAsync(body);

    //checking fields given.
    if (!(body.oldPassword && body.newPassword)) {
      throw new Error("Invalid Field");
    }
    // check if old and new password is not same
    if (body.oldPassword == body.newPassword) {
      throw new Error("Please Try Some Different Password");
    }

    const sequelize = app.get("sequelizeClient");
    const { users } = sequelize.models;

    const userObj = await users.findByPk(user.id);

    //check user is available in database.
    if (!userObj) {
      throw new Error("User Not Exist!");
    }

    const isAllow = await bcrypt.compare(body.oldPassword, userObj.password);
    //valid person or not.
    if (!isAllow) {
      throw new Error("You Are Not Valid Person Please Check Your Password.");
    }
    const hashedPassword = await bcrypt.hash(body.newPassword, 12);
    // update hashed-password.
    const updatedPassword = await users.update(
      { password: hashedPassword },
      { where: { id: user.id } }
    );
    //log message if update success or not.
    const Message = updatedPassword.toString() === "1" ? "Success." : "Fail";

    res.json({ Message: "Password Updated.", updatedPassword: Message });
  } catch (error) {
    console.error(error);
    return next(error);
  }
};
module.exports = changePassword;
