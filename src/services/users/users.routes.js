// const { authenticate } = require('@feathersjs/authentication');
// const validation = require('../../middleware/validation');
const { authenticate } = require('@feathersjs/express');
const eventsMethods = require("./methods");

module.exports = (app) => {

    app.post('/changePassword', authenticate('jwt'), eventsMethods.changePassword);

};
