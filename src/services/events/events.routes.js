// const { authenticate } = require('@feathersjs/authentication');
// const validation = require('../../middleware/validation');
const { authenticate } = require("@feathersjs/express");
const eventsMethods = require("./methods");

module.exports = app => {
  app.get("/my", authenticate("jwt"), eventsMethods.getMyEvents);

  app.post("/update", authenticate("jwt"), eventsMethods.updateEvent);

  app.post("/eventDetail", authenticate("jwt"), eventsMethods.getListOfUsers);

  //Searching & Sorting Path
  app.get("/list/sort", authenticate("jwt"), eventsMethods.list.sort);
  app.get("/list/search", authenticate("jwt"), eventsMethods.list.search);

  app.post(
    "/changePassword",
    authenticate("jwt"),
    eventsMethods.changePassword
  );
};
