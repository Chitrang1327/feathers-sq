const _ = require("lodash");
const Sequelize = require("sequelize");
const Op = Sequelize.Op;

// http://localhost:3030/my
// GET User(s)(Created By User) Events With Invited Event(s).
const getMyEvents = async (req, res, next) => {
  try {
    let allEvents = [];
    const { app, user } = req;
    const sequelize = app.get("sequelizeClient");
    const { events, invitations, users } = sequelize.models;

    //Created Events By User.
    const allCreatedEvents = await events.findAll({
      where: { user_id: user.id },
      include: [{ model: users, attributes: ["id", "email"] }],
      attributes: ["id", "user_id", "name", "description"]
    });
    allEvents.push(...allCreatedEvents);

    //All Invitations Details User Get invited in.
    const allInvitedEvents = await invitations.findAll({
      where: { invitation_to: user.id },
      include: [
        {
          model: events,
          attributes: ["id", "user_id", "name", "description"],
          include: [
            {
              model: users,
              attributes: ["id", "email"]
            }
          ]
        }
      ],
      attributes: ["invitation_from", "event_id"]
    });

    //#region  map_implementation
    // const invitedEventsId = _.map(allInvitedEvents, "event_id");
    // const invitedEvents = await events.findAll({
    //   where: {
    //     id: {
    //       [Op.in]: invitedEventsId
    //     }
    //   }
    // });
    // console.log("----->", invitedEvents);
    //#endregion

    //Simplified Events in Manner To Display.
    for (let e of allInvitedEvents) {
      if (e === null) continue;
      allEvents.push(e.event);
    }

    return res.json(allEvents);
  } catch (error) {
    console.error(error);
    throw new Error(error);
  }
};

module.exports = getMyEvents;
