const getMyEvents = require("./my_events");
const updateEvent = require("./update_event");
// const getListOfUsers = require("./event_details");
const getListOfUsers = require("./event_details_updated");
const list = require("./list");
const changePassword = require("../../users/methods/changePassword");

const Methods = {
  getMyEvents,
  updateEvent,
  getListOfUsers,
  list,
  changePassword
};

module.exports = Methods;
