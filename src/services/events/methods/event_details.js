const _ = require("lodash");
const errors = require("@feathersjs/errors");
const Schema = require("../../../middleware/validation");
const Sequelize = require("sequelize");
const Op = Sequelize.Op;

// http://localhost:3030/eventDetail
// GET list Of Users Invited in Event Id Given By Users.
const getListOfUsersInvited = async (req, res, next) => {
  try {
    const { app, user, body } = req;

    //joi input validations
    await Schema.eventDetailsSchema.validateAsync(body);

    const sequelize = app.get("sequelizeClient");
    const { invitations, users, events } = sequelize.models;

    //find event in database.
    const event = await events.findOne({
      where: { id: body.event_id },
      include: [
        {
          model: users,
          attributes: ["id", "email"]
        }
      ],
      attributes: ["id", "name"]
    });

    const invitedUsers = await invitations.findAll({
      where: { event_id: body.event_id },
      attributes: ["invitation_to", "event_id"],
      include: [
        {
          model: events,
          attributes: ["id", "name", "description"],
          include: [
            {
              model: users,
              attributes: ["id", "email"]
            }
          ]
        }
      ],
      raw: true
    });

    let modifiedObject;
    invitedUsers.forEach(element => {
      modifiedObject = {
        id: element.event_id,
        name: element["event.name"],
        description: element["event.description"],
        user: {
          id: element["event.user.id"],
          email: element["event.user.email"]
        }
      };
    });
    //throwing Error if Event not available in database.
    if (invitedUsers === undefined) {
      throw new Error("Event Not Found Please Check event_id");
    }
    //throwing Error if Event not available in database.
    else if (!modifiedObject) {
      throw new Error("Event Not Found Please Check event_id");
    }
    const user_id = _.map(invitedUsers, "invitation_to");
    // const event_id = _.map(invitedUsers, "event_id");

    const userObj = await users.findAll({
      where: {
        id: {
          [Op.in]: user_id
        }
      }
    });
    //list of users invited create in smp way.
    const invites = [];
    userObj.forEach(user => {
      let id = user.id;
      let email = user.email;
      invites.push({
        id,
        email
      });
    });

    res.json({
      event,
      invites
    });
  } catch (error) {
    console.error(error);
    return next(error);
  }
};

module.exports = getListOfUsersInvited;
