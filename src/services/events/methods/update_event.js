const Schema = require("../../../middleware/validation");

// http://localhost:3030/update
// Update Event.
const updateEvent = async (req, res, next) => {
  try {
    const { app, user, body } = req;

    const value = await Schema.updateSchema.validateAsync(body);

    //No need This Code we are checking inputs by using joi.
    // if (!(body.event_id && body.name && body.description)) {
    //   throw new Error("Invalid Parameters");
    // }

    const sequelize = app.get("sequelizeClient");
    const { events } = sequelize.models;
    const event = await events.findAll({
      where: { id: req.body.event_id, user_id: user.id }
    });

    //check event is available in database or not
    if (event.length === 0) {
      throw new Error(
        "Either Event is Not Belongs To You or Event Id is Wrong"
      );
    }
    //we find event in database update event.
    const updatedEvent = await events.update(
      {
        name: req.body.name,
        description: req.body.description
      },
      { where: { id: req.body.event_id, user_id: user.id } }
    );

    return res.json({ Message: "Event Updated Success." });
  } catch (error) {
    console.error(error);
    return next(error);
  }
};
module.exports = updateEvent;
