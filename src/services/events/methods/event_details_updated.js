const _ = require("lodash");
const errors = require("@feathersjs/errors");
const Schema = require("../../../middleware/validation");
const Sequelize = require("sequelize");
const Op = Sequelize.Op;

// http://localhost:3030/eventDetail
// GET list Of Users Invited in Event Id Given By Users.
const getListOfUsersInvited = async (req, res, next) => {
  try {
    const { app, user, body } = req;

    //joi input validations
    await Schema.eventDetailsSchema.validateAsync(body);

    const sequelize = app.get("sequelizeClient");
    const { invitations, users, events } = sequelize.models;

    //find event in database.
    const event = await events.findOne({
      where: { id: body.event_id },
      attributes: ["id", "name", "description"],
      include: [
        {
          model: users,
          raw: false,
          nested: true,
          attributes: ["id", "email"]
        },
        {
          model: invitations,
          attributes: ["id" /* , "invitation_to" */],
          include: [
            {
              model: users,
              attributes: ["id", "email"],
              raw: false,
              nested: true
            }
          ]
        }
      ],
      raw: false,
      nested: true
    });
    if (!event) {
      throw new Error("Event not Find With Given Id.");
    }
    res.json({
      event
    });
  } catch (error) {
    console.error(error);
    return next(error);
  }
};

module.exports = getListOfUsersInvited;
