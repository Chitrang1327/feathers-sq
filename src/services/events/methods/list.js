// http://localhost:3030/sort
//sort events by providing inputs

const Sequelize = require("sequelize");
const Op = Sequelize.Op;

const sort = async (req, res, next) => {
  try {
    const { app, user, body } = req;
    if (!body.sort_by) {
      throw new Error("Invalid Parameters");
    }
    const sequelize = app.get("sequelizeClient");
    const { events, users } = sequelize.models;
    const sorted = await events.findAll({
      order: [[body.sort_by, "ASC"]],
      include: [{ model: users, attributes: ["id", "email"] }],
      attributes: ["id", "name", "description"]
    });
    res.json({ Message: "Sort", sortedEvents: sorted });
  } catch (error) {
    console.error(error);
    return next(error);
  }
};

const search = async (req, res, next) => {
  try {
    const { app, user, body } = req;
    const sequelize = app.get("sequelizeClient");
    const { events } = sequelize.models;

    const sorted = await events.findAll({
      where: {
        name: {
          [Op.like]: "%body.name"
        }
      }
    });

    res.json({ sorted });
  } catch (error) {
    console.error(error);
    return next(error);
  }
};
module.exports = { sort, search };
