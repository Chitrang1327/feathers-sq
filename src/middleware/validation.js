const joi = require('@hapi/joi');

module.exports.updateSchema = joi.object({
    name: joi.string().min(3).max(300)
        .required(),
    description: joi.string().min(3).max(300)
        .required(),
    event_id: joi.number().integer()
        .required(),
});

module.exports.eventDetailsSchema = joi.object({
    event_id: joi.number().integer()
        .required(),
});

module.exports.changePasswordSchema = joi.object({
    oldPassword: joi.string().required()
        .pattern(/^[a-zA-Z0-9]{3,30}$/),
    newPassword: joi.string().required()
        .pattern(/^[a-zA-Z0-9]{3,30}$/)
});

