// See http://docs.sequelizejs.com/en/latest/docs/models-definition/
// for more of what you can do here.
const Sequelize = require("sequelize");
const DataTypes = Sequelize.DataTypes;
const joi = require("@hapi/joi");

module.exports = function(app) {
  const sequelizeClient = app.get("sequelizeClient");
  const events = sequelizeClient.define(
    "events",
    {
      name: {
        type: DataTypes.STRING,
        allowNull: false
      },
      description: {
        type: DataTypes.STRING,
        allowNull: false
      }
    },
    { underscored: true, timestamp: true },
    {
      hooks: {
        beforeCount(options) {
          options.raw = true;
        }
      }
    }
  );

  // eslint-disable-next-line no-unused-vars
  events.associate = function(models) {
    // Define associations here
    // See http://docs.sequelizejs.com/en/latest/docs/associations/
    events.belongsTo(models.users, {
      onDelete: "CASCADE",
      foreignKey: "user_id"
    });

    events.hasMany(models.invitations, { foreignKey: "event_id" });
  };
  return events;
};

module.exports.eventSchema = joi.object({
  name: joi
    .string()
    .min(3)
    .max(30)
    .required(),
  description: joi
    .string()
    .min(3)
    .max(300)
    .required()
});
