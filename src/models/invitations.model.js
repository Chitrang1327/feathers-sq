// See http://docs.sequelizejs.com/en/latest/docs/models-definition/
// for more of what you can do here.
const Sequelize = require("sequelize");
const DataTypes = Sequelize.DataTypes;
const joi = require("@hapi/joi");
module.exports = function(app) {
  const sequelizeClient = app.get("sequelizeClient");
  const invitations = sequelizeClient.define(
    "invitations",
    {
      invitation_from: {
        type: DataTypes.INTEGER,
        allowNull: false
      },
      invitation_to: {
        type: DataTypes.INTEGER,
        allowNull: false
      }
    },
    { underscored: true, timestamp: true },
    {
      hooks: {
        beforeCount(options) {
          options.raw = true;
        }
      }
    }
  );

  // eslint-disable-next-line no-unused-vars
  invitations.associate = function(models) {
    // Define associations here
    // See http://docs.sequelizejs.com/en/latest/docs/associations/
    invitations.belongsTo(models.events);
    invitations.belongsTo(models.users);
  };

  return invitations;
};

module.exports.invitationSchema = joi.object({
  invitation_to: joi
    .number()
    .integer()
    .required(),
  event_id: joi
    .number()
    .integer()
    .required()
});
