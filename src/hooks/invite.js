// Use this hook to manipulate incoming or outgoing data.
// For more information on hooks see: http://docs.feathersjs.com/api/hooks.html

// eslint-disable-next-line no-unused-vars
const schema = require("../models/invitations.model");

// http://localhost:3030/invitations
// POST : Send invitation To User in Event(s).
module.exports = (options = {}) => {
  try {
    return async context => {
      // joi Validations for input.
      await schema.invitationSchema.validateAsync(context.data);

      //custom Validation For checking Inputs.
      if (context.data.invitation_to === "") {
        throw new Error("Empty Field Not Allow.");
      }
      if (
        context.data.invitation_to.toString() ===
        context.params.user.id.toString()
      ) {
        throw new Error("You Can't Invite Your Self");
      }

      // const users = context.app.service('users');
      // const user = await users.find({ query: { email: context.data.invitationTo } });

      const sequelize = context.app.get("sequelizeClient");
      const { users, events, invitations } = sequelize.models;
      const invitation = await invitations.findOne({
        where: {
          invitation_to: context.data.invitation_to,
          event_id: context.data.event_id
        }
      });

      if (invitation) {
        throw new Error("Invitation Already Sent.");
      }

      // for checking in database user and event with given id is available or not.
      const user = await users.findAll({
        where: { id: context.data.invitation_to }
      });
      const event = await events.findAll({
        where: { id: context.data.event_id, user_id: context.params.user.id }
      });

      // if (user === undefined) {
      //   throw new Error("can't Find User With This Email.")
      // }
      if (user.length === 0) {
        throw new Error("Can't Find User With This id.");
      } else if (event.length === 0) {
        throw new Error(
          "Can't Find Event With This event_id Or Not Created By You."
        );
      }

      context.data.invitation_from = context.params.user.id;
      context.data.event_id = context.data.event_id;
      context.data.user_id = context.data.invitation_to;

      return context;
    };
  } catch (error) {
    console.error(error);
    return next(error);
  }
};
