// Use this hook to manipulate incoming or outgoing data.
// For more information on hooks see: http://docs.feathersjs.com/api/hooks.html

// eslint-disable-next-line no-unused-vars
const faker = require("faker");

//http://localhost:3030/events
//POST : Create new Event.
module.exports = (options = {}) => {
  return async context => {
    //Random Creation Data By Using faker.
    if (!!context.params.query.faker) {
      context.data.name = faker.random.word();
      context.data.description = faker.random.words();
      context.data.invitations_To = [null];
    }
    //Pass user_id foreignKey in Event DataBase
    context.data.user_id = context.params.user.id;
    return context;
  };
};
